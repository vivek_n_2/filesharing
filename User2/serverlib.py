#! /usr/bin/env python

import socket
import subprocess
import hashlib
import os
import re
import datetime
import magic 
import time

md5 = hashlib.md5()

class SERVER:

    def __init__(self,sockettype):
        if sockettype == 1:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        else:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def bind(self, host, port):
        self.sock.bind((host, port))

    def get_files(self,dir):

        allfiles = os.walk(dir)
        result = []
        for root, dirs, files in allfiles:
            for each in files:
                result.append(os.path.join(root,each))
        return result

    def shortlist(self,start,end):
            try:
                start = time.mktime(datetime.datetime.strptime(start, "%Y-%m-%d %H:%M:%S").timetuple())
                end = time.mktime(datetime.datetime.strptime(end, "%Y-%m-%d %H:%M:%S").timetuple())
                print start,end
                p = self.get_files(".")
                ok = False
            except:
                self.send_message("Invalid Arguments to IndexGet")
                self.terminate()
            else:
                if len(p) == 0:
                    self.send_message("No files.")
                    self.terminate()
                else:
                    for line in p:
                        filename = line.strip("\n")
                        modtimestamp = os.stat(filename).st_mtime
                        mtime = datetime.datetime.fromtimestamp(int(modtimestamp)).strftime('%Y-%m-%d %H:%M:%S')
                        if modtimestamp >= start and modtimestamp <= end:
                            self.send_message("File: %s, Size: %d bytes, Last Modified: %s, Type: %s\n"%(filename,os.path.getsize(filename),mtime,magic.from_file(filename)))
                            ok = True
                    if not ok:
                        self.send_message("No files.")
                    self.terminate()

    def longlist(self):
        p = self.get_files(".")
        for line in p:
            filename = line.strip('\n')
            mtime = datetime.datetime.fromtimestamp(int(os.stat(filename).st_mtime)).strftime('%Y-%m-%d %H:%M:%S')
            self.send_message("File: %s, Size: %d bytes, Last Modified: %s, Type: %s\n"%(filename,os.path.getsize(filename),mtime,magic.from_file(filename)))
        self.terminate()

    def regex(self,reg):
        try:
            reg = "^"+reg+"$"
            re.compile(reg)
        except Exception as inst:
            self.send_message("Invalid Regex.")
            self.terminate()
            return False
        else:
            p = self.get_files(".")
            for line in p:
                filename = line.strip('\n')
                mtime = datetime.datetime.fromtimestamp(int(os.stat(filename).st_mtime)).strftime('%Y-%m-%d %H:%M:%S')
                if(re.match(reg,filename)):
                    self.send_message("File: %s, Size: %d bytes, Last Modified: %s, Type: %s\n"%(filename,os.path.getsize(filename),mtime,magic.from_file(filename)))
            self.terminate()
            return True
     
    def terminate(self):
        self.send_message("#"+"0"+"0"+"1")    
    
    def hash(self,filename):
        return hashlib.md5(open(filename, 'rb').read()).hexdigest()

    def RunCommand(self, cmd):
        cmd = cmd.split(' ')
        cnt = len(cmd)
        if cnt == 0:
            self.send_message("Command cannot be empty. Try using IndexGet, FileHash, FileDownload.")
            self.terminate()
            return False

        command = cmd[0]
        if command == "IndexGet":
            if cnt == 1:
                self.send_message("Please specify flags. Try using shortlist, longlist, regex.")
                self.terminate()
                return False
            flag = cmd[1]
            if flag == "shortlist":
                if cnt!=6:
                    self.send_message("usage: IndexGet shortlist <start TimeStamp> yyyy-mm-dd HH:MM:SS <end TimeStamp> yyyy-mm-dd HH:MM:SS.")
                    self.terminate()
                    return False
                else:
                    start = "%s %s"%(cmd[2], cmd[3])
                    end = "%s %s"%(cmd[4], cmd[5])
                    return self.shortlist(start,end)

            elif flag == "longlist":
                if cnt!=2:
                    self.send_message("Invalid Arguments.")
                    self.terminate()
                    return False
                return self.longlist()
            elif flag == "regex":
                if cnt!=3:
                    self.send_message("usage: IndexGet regex <regex>")
                    self.terminate()
                    return False
                return self.regex(cmd[2])
            else:
                 self.send_message("Invalid flags to IndexGet. Try using shortlist, longlist, regex.")
                 self.terminate()

        elif command == "FileHash":
            if cnt == 1:
                self.send_message("Please specify flags. Try using verify, checkall")
                self.terminate()
                return False
            flag = cmd[1]
            if flag == "verify":
                if cnt != 3:
                    self.send_message("usage: FileHash verify < file name > .")
                    self.terminate()
                    return False
                filename = cmd[2]
                if not os.path.isfile(filename):
                    self.send_message("Required File Doesn't Exist.")
                    self.terminate()
                    return False
                else:
                    self.send_message("#"+"0"+"0"+"2")
                    self.send_message("File : %s , MD5hash : %s\n"%(filename,self.hash(filename)))
                
                self.terminate()
                return True
            elif flag == "checkall":
                if cnt != 2:
                    self.send_message("usage: FileHash checkall .")
                    self.terminate()
                    return False
                self.send_message("#"+"0"+"0"+"2")
                for file in self.get_files("."):
                    filename = file.split('/')[-1]
                    self.send_message("File : %s , MD5hash : %s\n"%(filename,self.hash(filename)))
                    
                self.terminate()
                return True
            else:
                self.send_message("Invalid flags to FileHash. Try using verify, checkall.")
                self.terminate()
                return False

        elif command == "FileDownload":
            if cnt == 1:
                self.send_message("Please specify flags. Try using TCP, UDP")
                self.terminate()
                return False
            flag = cmd[1]
            if cnt != 3:
                self.send_message("usage: FileHash TCP < file name > .")
                self.terminate()
                return False
            filename = cmd[2]
            if not os.path.isfile(filename):
                self.send_message("Required File Doesn't Exist.")
                self.terminate()
                return False
            self.send_message("File Successfully sent\n")
            mtime = datetime.datetime.fromtimestamp(int(os.stat(filename).st_mtime)).strftime('%Y-%m-%d %H:%M:%S')
            self.send_message("File : %s, Size : %d bytes, Last Modified at %s ,MD5hash : %s\n"%(filename,os.path.getsize(filename),mtime,self.hash(filename)))
            self.send_message("#"+"0"+"0"+"0")
            self.sendFile(filename)
            self.terminate()
            return True

        else:
            self.send_message("Invalid Command. Try using IndexGet, FileHash, FileDownload.")
            self.terminate()
            return False

    def end(self):
        self.sock.close()

 
class TCPSERVER(SERVER):
    def __init__(self):
        SERVER.__init__(self,1)
 
    def queLimit(self, a):
        self.sock.listen(a)
 
    def accept(self):
        self.clientsocket, self.clientaddress = self.sock.accept()
        print "Connected to",self.clientaddress
 
 
    def sendFile(self,File):
        fptr = open(File, "rb") 
        data = fptr.read(1024)
        while(data):
            self.clientsocket.send(data)
            data = fptr.read(1024)
        fptr.close()
 
    def send_message(self,msg):
        self.clientsocket.send(msg)
 
    def get_message(self):
        print "Hello hi"
        cmd = self.clientsocket.recv(1024)
        self.RunCommand(cmd)
 
class UDPSERVER(SERVER):
    def __init__(self):
        SERVER.__init__(self,2)
 
    def queLimit(self, a):
        pass
 
    def accept(self):
        pass
 
    def sendFile(self,File):
        fptr = open(File, "rb") 
        data = fptr.read(1024)
        while(data):
            self.sock.sendto(data,self.clientaddress)
            data = fptr.read(1024)
        fptr.close()
 
    def send_message(self,msg):
        self.sock.sendto(msg,self.clientaddress)
 
    def get_message(self):
        cmd,self.clientaddress = self.sock.recvfrom(1024)
        self.RunCommand(cmd)

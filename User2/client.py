#! /usr/bin/env python
 
import clientlib
 
while True:
    command = raw_input("$> ")
    if(command=="exit"):
    	break
    if "UDP" in command:
        sock = clientlib.UDPCLIENT(command)
        sock.end()
    else:
        sock = clientlib.TCPCLIENT(command)
        sock.end()
 
exit(0)

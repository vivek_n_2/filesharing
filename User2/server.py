#! /usr/bin/env python
 
import serverlib
import threading
 
global exitFlag
exitFlag = False
 
class myThread(threading.Thread):
 
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
 
        if name == "TCP":
            self.sock = serverlib.TCPSERVER()
            f = open('TCPserver.txt', 'rw+')
            data = f.readlines()
            HOST = data[0].strip('\n')
            PORT = int(data[1].strip('\n'))
            self.sock.bind(HOST, PORT)
            self.sock.queLimit(5)
            f.close()
        elif name == "UDP":
            self.sock = serverlib.UDPSERVER()
            f = open('UDPserver.txt', 'rw+')
            data = f.readlines()
            HOST = data[0].strip('\n')
            PORT = int(data[1].strip('\n'))
            self.sock.bind(HOST, PORT)
            self.sock.queLimit(5)
            f.close()

    def run(self):
    	if(self.name=="main"):
    		a=raw_input()
    		if(a=="exit"):
    			global exitFlag
    			exitFlag = True
    	else:
	        while True:
	            self.sock.accept()
	            self.sock.get_message()
    def end(self):
    	self.sock.end()
 
thread1 = myThread(1, "TCP")
thread1.daemon = True
thread2 = myThread(2, "UDP")
thread2.daemon = True
thread3 = myThread(3, "main") 
thread1.start()
thread2.start()
thread3.start()
 
while True:
	if exitFlag:
		print "Closing ...BYE BYE"
		thread1.end()
		thread2.end()
		thread3.join()
		break
   	else:
   		pass
 
exit(0)
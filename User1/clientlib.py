#! /usr/bin/env python

import socket
import re
import MySQLdb
import warnings

class CLIENT:

    def __init__(self,sockettype):
        if sockettype == 1:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        else:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.filetransfer = False
        warnings.filterwarnings('ignore', category=MySQLdb.Warning)
        self.db=MySQLdb.connect("localhost","Client","networks","FileSharing1")
        self.cursor=self.db.cursor()
        self.cursor.execute("CREATE TABLE IF NOT EXISTS Files (Name varchar(100),md5 varchar(100))")

    def run_command(self,cmd):
        if cmd.split(' ')[0]=="FileDownload":
            self.filetransfer = True
        self.send_message(cmd)

    def receive_output(self):
        message = ""
        while True:
            if len(message)>4 and message[-4:]=="#"+"0"+"0"+"1":
                break
            line = self.recv_message(8194)
            message += line
        parts = re.split("#"+"0"+"0"+"0",message)
        parts2 = re.split("#"+"0"+"0"+"2",message)
        if len(parts)==1 and len(parts2)==1:
            message = message[:-4].strip('\n')
            for i in message.split('\n'):
                print i.strip('\n')
        elif len(parts2)==1:
            self.db=MySQLdb.connect("localhost","Client","networks","FileSharing1")
            self.cursor=self.db.cursor()
            reply = parts[0].split("\n")
            filename = reply[1].split(',')[0].split(':')[1].strip().split("/")[-1] 
            hashvalue = reply[1].split(',')[-1].split(':')[1].strip()
            self.cursor.execute("SELECT md5 from Files where Files.Name=%s",(filename));
            output = self.cursor.fetchall();
            if(len(output)>0 and output[0][0]!=hashvalue):
                self.cursor.execute("UPDATE Files SET md5=%s where Name=%s;",(hashvalue,filename))
            elif(len(output)==0):    
                self.cursor.execute("INSERT INTO Files values(%s,%s);",(filename,hashvalue))
            self.db.commit()
            fptr = open(filename,"wb")
            fptr.write(parts[1][:-4])
            fptr.close()
            print reply[1]
            self.db.close()
            return True
        else:
            self.db=MySQLdb.connect("localhost","Client","networks","FileSharing1")
            self.cursor=self.db.cursor()
            details = parts2[-1][:-4].split("\n")
            ok = False
            for each in details:
                if not each:
                    continue
                filename = each.split(',')[0].split(':')[-1].strip()
                hashvalue = each.split(',')[1].split(':')[-1].strip()
                self.cursor.execute("SELECT md5 from Files where Files.Name=%s",(filename));
                output = self.cursor.fetchall();
                if(len(output)>0 and output[0][0]!=hashvalue):
                    ok = True
                    print "File %s is modified"%(filename)
            if not ok:
                    print "Required files are up-to-date."
            self.db.commit()
            self.db.close()

    def end(self):
        self.sock.close()

class TCPCLIENT(CLIENT):
    def __init__(self,command):
        CLIENT.__init__(self,1)
        f = open('TCPclient.txt', 'rw+')
        data = f.readlines()
        HOST = data[0].strip('\n')
        PORT = int(data[1].strip('\n'))
        f.close()
        self.connect(HOST, PORT)
        self.filetransfer = False
        self.run_command(command)
        self.receive_output()
 
    def connect(self, host, port):
        self.host = host
        self.port = port
        self.sock.connect((host, port))
 
    def send_message(self, msg):
        self.sock.send(msg)
 
    def recv_message(self, msg):
        return self.sock.recv(msg)
 
class UDPCLIENT(CLIENT):
    def __init__(self,command):
        CLIENT.__init__(self,2)
        f = open('UDPclient.txt', 'rw+')
        data = f.readlines()
        HOST = data[0].strip('\n')
        PORT = int(data[1].strip('\n'))
        self.connect(HOST, PORT)
        f.close()
        self.filetransfer = False
        self.run_command(command)
        self.receive_output()
 
    def connect(self, host, port):
        self.host = (host, port)
 
    def send_message(self, msg):
        self.sock.sendto(msg,self.host)
 
    def recv_message(self, msg):
        msg, self.host=self.sock.recvfrom(msg)
        return msg
